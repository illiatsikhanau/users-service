import type {NextApiRequest, NextApiResponse} from 'next';
import prisma from '@/lib/prisma';

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    if (req.method === 'GET') await getUsers(req, res);
    else if (req.method === 'POST') await addUser(req, res);
    else if (req.method === 'PUT') await editUser(req, res);
    else methodNotAllowed(req, res);

}

const getUsers = async (req: NextApiRequest, res: NextApiResponse) => {
    try {
        const users = await prisma.user.findMany({
            orderBy: [
                {id: 'asc'},
            ],
        });
        res.send(users);
    } catch {
        badRequest(req, res);
    }
}

const addUser = async (req: NextApiRequest, res: NextApiResponse) => {
    const {name, surname, city, age} = JSON.parse(req.body);
    try {
        const user = await prisma.user.create({
            data: {
                name: name,
                surname: surname,
                city: city,
                age: Number(age),
            },
        });
        await sendHistory('add', user);
        res.send(user);
    } catch {
        badRequest(req, res);
    }
}

const editUser = async (req: NextApiRequest, res: NextApiResponse) => {
    const {id, name, surname, city, age} = JSON.parse(req.body);
    try {
        const user = await prisma.user.update({
            where: {
                id: id
            },
            data: {
                name: name,
                surname: surname,
                city: city,
                age: Number(age),
            },
        });
        await sendHistory('edit', user);
        res.send(user);
    } catch {
        badRequest(req, res);
    }
}

const sendHistory = async (
    action: string,
    user: { id: string, name: string, surname: string, city: string, age: number }
) => {
    try {
        await fetch(`${process.env.HISTORY_SERVICE_URL}/api/history`, {
            method: 'POST',
            body: JSON.stringify({
                userId: user.id,
                action: 'edit',
                post: JSON.stringify({
                    name: user.name,
                    surname: user.surname,
                    city: user.city,
                    age: user.age
                }),
            }),
        });
    } catch {
    }
}

const badRequest = (req: NextApiRequest, res: NextApiResponse) =>
    res.status(400).send('Bad request');
const methodNotAllowed = (req: NextApiRequest, res: NextApiResponse) =>
    res.status(405).send('Method Not Allowed');


export default handler;
