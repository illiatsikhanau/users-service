export interface History {
    id: string,
    userId: string,
    action: string,
    post: string,
    createdAt: string,
}
