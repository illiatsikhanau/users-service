export interface User {
    id: string,
    name: string,
    surname: string,
    city: string,
    age: string,
    changed?: boolean,
}

export const DEFAULT_USER: User = {
    id: '',
    name: '',
    surname: '',
    city: '',
    age: '',
    changed: false
}
