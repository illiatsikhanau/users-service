'use client'

import style from '../styles/Home.module.scss';
import {ChangeEvent, KeyboardEvent, useEffect, useState} from 'react';
import {Alert, AlertIcon, AlertTitle, Button, Heading, Input, Textarea, useNumberInput} from '@chakra-ui/react';
import {DEFAULT_USER, User} from '@/entities/User';
import {History} from '@/entities/History';

const DEFAULT_HISTORY = {
    list: [] as History[],
    page: 1,
    pages: 1,
};
const HISTORY_LIMIT = 5;

const Home = () => {
    const [isLoading, setIsLoading] = useState(false);

    const [users, setUsers] = useState([] as User[]);
    const updateUserField = (e: ChangeEvent<HTMLInputElement>, id: string, fieldName: string) => {
        setUsers(users.map(user => user.id === id ? {
            ...user,
            [fieldName]: fieldName === 'age' ? e.target.value.replace('e', '') : e.target.value,
            changed: true
        } : user));
        setIsError(false);
    }

    const [history, setHistory] = useState(DEFAULT_HISTORY);

    const [newUser, setNewUser] = useState({...DEFAULT_USER});
    const [isError, setIsError] = useState(false);
    const updateNewUserField = (e: ChangeEvent<HTMLInputElement>, fieldName: string) => {
        setNewUser({
            ...newUser,
            [fieldName]: fieldName === 'age' ? e.target.value.replace('e', '') : e.target.value
        });
        setIsError(false);
    }

    const addUser = () => {
        setIsLoading(true);
        fetch('/api/users', {
            method: 'POST',
            body: JSON.stringify(newUser),
        }).then(async res => {
            if (res.ok) {
                setNewUser({...DEFAULT_USER});
                const user = await res.json();
                setUsers([...users, user as User]);
            }
            setIsError(!res.ok);
        }).finally(() => {
            setIsLoading(false);
        });
    }

    const updateUser = (user: User) => {
        setIsLoading(true);
        fetch('/api/users', {
            method: 'PUT',
            body: JSON.stringify(user),
        }).then(async res => {
            if (res.ok) {
                setNewUser({...DEFAULT_USER});
                const user = await res.json();
                setUsers(users.map(u => u.id === user.id ? user : u));
            }
            setIsError(!res.ok);
        }).finally(() => {
            setIsLoading(false);
            setHistory({...history, list: []});
        });
    }

    const toggleEditingHistory = (user: User) => {
        if (hasHistory(user)) {
            setHistory(DEFAULT_HISTORY);
        } else {
            getHistory(user);
        }
    }

    const getHistory = (user: User, page?: number) => {
        fetch(`${process.env.HISTORY_SERVICE_URL}/api/history/${user.id}?limit=${HISTORY_LIMIT}&page=${page ?? history.page}`)
            .then(async res => {
                const data = await res.json();
                setHistory({
                    ...data,
                    page: page ?? history.page,
                    pages: Math.ceil(data.length / HISTORY_LIMIT),
                });
            });
    }
    const hasHistory = (user: User) => history.list.find(history => history.userId === user.id);
    const setPage = async (user: User, page: number) => getHistory(user, page);

    const onNewUserEnter = (e: KeyboardEvent<HTMLInputElement>) => {
        if (e.key === 'Enter') addUser();
    }
    const onUpdateUserEnter = (e: KeyboardEvent<HTMLInputElement>, user: User) => {
        if (e.key === 'Enter') updateUser(user);
    }

    useEffect(() => {
        fetch('api/users').then(async res => {
            const users = await res.json();
            setUsers(users);
        });
    }, []);

    const {getInputProps} = useNumberInput({name: 'age'});
    const ageInput = getInputProps();

    return (
        <main className={style.home}>
            <Heading as='h1' size='xl'>Users Service</Heading>
            {isError ?
                <Alert status='error'>
                    <AlertIcon/>
                    <AlertTitle>Sorry, something went wrong. Please try again later.</AlertTitle>
                </Alert>
                : ''
            }
            <div className={style.section}>
                <Heading as='h2' size='md'>Add New User</Heading>
                <div className={style.row}>
                    <Input
                        name='name'
                        autoComplete='off'
                        placeholder='Name'
                        maxLength={255}
                        value={newUser.name}
                        onChange={e => updateNewUserField(e, 'name')}
                        onKeyUp={onNewUserEnter}
                        isDisabled={isLoading}
                    />
                    <Input
                        name='surname'
                        autoComplete='off'
                        placeholder='Surname'
                        maxLength={255}
                        value={newUser.surname}
                        onChange={e => updateNewUserField(e, 'surname')}
                        onKeyUp={onNewUserEnter}
                        isDisabled={isLoading}
                    />
                    <Input
                        name='city'
                        autoComplete='off'
                        placeholder='City'
                        maxLength={255}
                        value={newUser.city}
                        onChange={e => updateNewUserField(e, 'city')}
                        onKeyUp={onNewUserEnter}
                        isDisabled={isLoading}
                    />
                    <Input
                        placeholder='Age'
                        type='number'
                        {...ageInput}
                        value={newUser.age}
                        onChange={e => updateNewUserField(e, 'age')}
                        onKeyUp={onNewUserEnter}
                        isDisabled={isLoading}
                    />
                    <Button
                        colorScheme='blue'
                        width='100%'
                        onClick={addUser}
                        isDisabled={isLoading}
                    >
                        Add User
                    </Button>
                </div>
            </div>
            <div className={style.section}>
                <Heading as='h2' size='md'>All Users</Heading>
                {users.map(user =>
                    <div key={user.id} className={style.user}>
                        <div className={style.row}>
                            <Input
                                name='name'
                                autoComplete='off'
                                placeholder='Name'
                                maxLength={255}
                                value={user.name}
                                onChange={e => updateUserField(e, user.id, 'name')}
                                onKeyUp={e => onUpdateUserEnter(e, user)}
                                isDisabled={isLoading}
                            />
                            <Input
                                name='surname'
                                autoComplete='off'
                                placeholder='Surname'
                                maxLength={255}
                                value={user.surname}
                                onChange={e => updateUserField(e, user.id, 'surname')}
                                onKeyUp={e => onUpdateUserEnter(e, user)}
                                isDisabled={isLoading}
                            />
                            <Input
                                name='city'
                                autoComplete='off'
                                placeholder='City'
                                maxLength={255}
                                value={user.city}
                                onChange={e => updateUserField(e, user.id, 'city')}
                                onKeyUp={e => onUpdateUserEnter(e, user)}
                                isDisabled={isLoading}
                            />
                            <Input
                                placeholder='Age'
                                {...ageInput}
                                value={user.age}
                                onChange={e => updateUserField(e, user.id, 'age')}
                                onKeyUp={e => onUpdateUserEnter(e, user)}
                                isDisabled={isLoading}
                            />
                            <Button
                                colorScheme='blue'
                                variant='outline'
                                width='100%'
                                onClick={() => toggleEditingHistory(user)}
                                isDisabled={isLoading}
                            >
                                {hasHistory(user) ? 'Hide History' : 'Show History'}
                            </Button>
                            <Button
                                colorScheme='blue'
                                width='100%'
                                onClick={() => updateUser(user)}
                                isDisabled={isLoading || !user.changed}
                            >
                                Update Data
                            </Button>
                        </div>
                        {hasHistory(user) ?
                            <div className={style.history}>
                                <div className={style.row}>
                                    <Heading as='h3' size='sm' width='100%'>Action</Heading>
                                    <Heading as='h3' size='sm' width='100%'>Data</Heading>
                                    <Heading as='h3' size='sm' width='100%'>Time</Heading>
                                </div>
                                {history.list.map(h =>
                                    <div key={h.id} className={style.row}>
                                        <Input defaultValue={h.action}/>
                                        <Textarea
                                            defaultValue={h.post}
                                            size='sm'
                                            width='100%'
                                        />
                                        <Input defaultValue={new Date(h.createdAt).toLocaleString()}/>
                                    </div>
                                )}
                                <div className={style.pages}>
                                    {history.page > 1 ?
                                        <Button
                                            colorScheme='blue'
                                            variant='outline'
                                            onClick={() => setPage(user, 1)}
                                        >
                                            1
                                        </Button>
                                        : ''
                                    }
                                    {history.page > 2 ?
                                        <>
                                            <span>...</span>
                                            <Button
                                                colorScheme='blue'
                                                variant='outline'
                                                onClick={() => setPage(user, history.page - 1)}
                                            >
                                                Prev
                                            </Button>
                                        </>
                                        : ''
                                    }
                                    <Button colorScheme='blue'>{history.page}</Button>
                                    {history.page + 1 < history.pages ?
                                        <>
                                            <Button
                                                colorScheme='blue'
                                                variant='outline'
                                                onClick={() => setPage(user, history.page + 1)}
                                            >
                                                Next
                                            </Button>
                                            <span>...</span>
                                        </>
                                        : ''
                                    }
                                    {history.page < history.pages ?
                                        <Button
                                            colorScheme='blue'
                                            variant='outline'
                                            onClick={() => setPage(user, history.pages)}
                                        >
                                            {history.pages}
                                        </Button>
                                        : ''
                                    }
                                </div>
                            </div>
                            : ''
                        }
                    </div>
                )}
            </div>
        </main>
    );
}

export default Home;
