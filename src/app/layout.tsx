import '../styles/main.css';
import type {Metadata} from 'next';
import {Inter} from 'next/font/google';
import {ChildrenProps} from '@/props/ChildrenProps';
import Providers from '@/app/providers';

const inter = Inter({subsets: ['latin']});

export const metadata: Metadata = {
    title: 'Users Service',
    description: 'Users Service',
}

const RootLayout = (props: ChildrenProps) => {
    return (
        <html lang='en'>
        <body className={inter.className}>
        <Providers>
            {props.children}
        </Providers>
        </body>
        </html>
    );
}

export default RootLayout;
