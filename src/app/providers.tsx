'use client'

import {ChildrenProps} from '@/props/ChildrenProps';
import {CacheProvider} from '@chakra-ui/next-js';
import {ChakraProvider} from '@chakra-ui/react';

const Providers = (props: ChildrenProps) => {
    return (
        <CacheProvider>
            <ChakraProvider>
                {props.children}
            </ChakraProvider>
        </CacheProvider>
    );
}

export default Providers;
