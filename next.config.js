/** @type {import('next').NextConfig} */

module.exports = {
    reactStrictMode: true,
    env: {
        HISTORY_SERVICE_URL: process.env.HISTORY_SERVICE_URL,
    }
}
